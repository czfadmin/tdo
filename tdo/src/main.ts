import { NestFactory } from '@nestjs/core';
import { fastifyHelmet } from 'fastify-helmet';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import {
	FastifyAdapter,
	NestFastifyApplication
} from "@nestjs/platform-fastify";

import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter(), {
		logger: ['error', 'warn'],
	})

	// helmet
	await app.register(fastifyHelmet)
	await app.listen(3000);

	// swagger
	const config = new DocumentBuilder()
		.setTitle('Cats example')
		.setDescription('The cats API description')
		.setVersion('1.0')
		.addTag('cats')
		.build();
	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup('api', app, document);

	console.log(`App has started and runing on:${await app.getUrl()}`)
}
bootstrap();
