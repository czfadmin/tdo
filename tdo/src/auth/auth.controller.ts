import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import CreateAccountDto from 'src/account/dto/create-account.dto';
import { LoginAccountDto } from 'src/account/dto/login-account.dto';
import { AuthService } from './auth.service';
import { Public } from './decorators';
import { LocalAuthGuard } from './local-auth.guard';

@Controller('api/auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @UseGuards(LocalAuthGuard)
    @Post('login')
    @Public()
    login(@Body() loginAccountDto: LoginAccountDto) {
        return this.authService.login(loginAccountDto);
    }

    @Post('register')
    register(@Body() createAccountDto: CreateAccountDto) {
        return this.authService.register(createAccountDto);
    }
}
