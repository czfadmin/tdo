import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AccountService } from 'src/account/account.service';
import CreateAccountDto from 'src/account/dto/create-account.dto';
import { LoginAccountDto } from 'src/account/dto/login-account.dto';
import { CommonResult } from 'src/common/common.result';

@Injectable()
export class AuthService {
    constructor(
        private accountService: AccountService,
        private jwtService: JwtService,
    ) {}

    async validateUser(username: string, password: string) {
        const user = await this.accountService.findOne(username);
        if (user && user.password === password) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async login(accountDto: LoginAccountDto) {
        const { username } = accountDto;
        if (username !== undefined && accountDto.password !== undefined) {
            let user = await this.accountService.findOne(username);
            if (user !== undefined && user.password === accountDto.password) {
                const { password, ...result } = user;
                const payload = {
                    username: user.username,
                    sub: user.id,
                    ...result,
                };
                const token = {
                    access_token: this.jwtService.sign(payload),
                };
                return CommonResult.Success(token, 'Login Successfully');
            } else {
                return CommonResult.Failed('Login Failed');
            }
        }
        return CommonResult.Failed('Login Failed');
    }

    async register(dto: CreateAccountDto) {
        const { username, email } = dto;
        let existedUser = await this.accountService.findOne(username, email);
        if (existedUser) {
            return CommonResult.Failed('User has existed!');
        }
        // TODO:加密密码
        if (dto.password === dto.passwordConfirm) {
            let newAccount = this.accountService.create(dto);
            if (newAccount !== undefined) {
                const { password, ...result } = newAccount;
                return CommonResult.Success(result, 'Register Successfully!');
            }
            return CommonResult.Failed('Register Failed!');
        }
        return CommonResult.Failed('两次密码不同，注册失败');
    }
}
