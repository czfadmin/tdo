import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Todo {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({
		nullable: false
	})
	title: string;

	@Column()
	content: string;
	@Column({
		type: "boolean",
		default: false
	})
	completed: boolean;

	@Column({
		type: "datetime"
	})
	lastUpdated: Date;

	@Column({
		type: 'text',
	})
	memo: string;
}
