import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Todo } from './entities/todo.entity';

@Injectable()
export class TodoService {
	constructor(@InjectRepository(Todo) private todoRepo: Repository<Todo>) {

	}
	create(createTodoDto: CreateTodoDto) {
		return this.todoRepo.create(createTodoDto);
	}

	findAll() {
		return this.todoRepo.find();
	}

	findOne(id: number) {
		return this.todoRepo.findOne(id)
	}



	async update(id: number, updateTodoDto: UpdateTodoDto) {
		let old = await this.todoRepo.findOne(id);
		if (old == null) {
			return null;
		}
		// old.title = updateTodoDto.title;
		// old.content = updateTodoDto.content;
		// old.memo = updateTodoDto;
		return this.todoRepo.update(id, updateTodoDto)
	}

	async remove(id: number) {
		let existed = await this.todoRepo.findOne(id)
		if (existed != null) {
			return this.todoRepo.remove(existed)
		}
		return null;
	}
}
