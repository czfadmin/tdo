import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import CreateAccountDto from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { Account } from './entities/account.entity';

@Injectable()
export class AccountService {
	findOneByEmail(email: string) {
		return this.accountRepo.findOne(email)
	}
	login(username: string, password: string): Promise<Account | undefined> {
		let user = this.accountRepo.findOne({
			select: ['username'], where: it => {
				it.username == username
			}
		});
		return user;
	}

	constructor(@InjectRepository(Account) private accountRepo: Repository<Account>) {

	}

	register(createAccountDto: CreateAccountDto) {

	}

	create(createAccountDto: CreateAccountDto) {
		let account = this.accountRepo.create(createAccountDto);
		this.accountRepo.save(account, {

		})
		return account;
	}

	findAll() {
		return `This action returns all account`;
	}


	findOne(username: string, email?: string): Promise<Account | undefined> {
		return this.accountRepo.findOne({
			username: username,
			email: email
		});;
	}
	findOneById(id: number) {
		return this.accountRepo.findOne(id)
	}
	update(id: number, updateAccountDto: UpdateAccountDto) {
		return `This action updates a #${id} account`;
	}

	remove(id: number) {
		return `This action removes a #${id} account`;
	}
}
