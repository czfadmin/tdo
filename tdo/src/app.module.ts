import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AccountModule } from './account/account.module';
import { TodoModule } from './todo/todo.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ThrottlerModule } from '@nestjs/throttler';
import { Connection } from 'typeorm';
import { CommonModule } from './common/common.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { APP_GUARD } from '@nestjs/core/constants';

@Module({
    imports: [
        // AccountModule,
        AuthModule,
        TypeOrmModule.forRoot(),
        TodoModule,
        ThrottlerModule.forRoot({
            ttl: 60,
            limit: 10,
        }),
        CommonModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        {
            provide: APP_GUARD,
            useClass: JwtAuthGuard,
        },
    ],
})
export class AppModule {
    constructor(private connection: Connection) {}
}
