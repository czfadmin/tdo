import { PartialType } from '@nestjs/mapped-types';
export default class CreateAccountDto {
	username: string;
	password: string;
	passwordConfirm: string;
	email: string;

	phone: Partial<string> | '';

	memo: Partial<string> | '';


}
