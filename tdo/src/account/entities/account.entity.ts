import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Account {

	@PrimaryGeneratedColumn()
	id: string;
	@Column()
	username: string;
	@Column({
		default: ''
	})
	email: string;
	@Column()
	password: string;
	@Column({
		default: ''
	})
	memo: string | '';
	@Column({
		default: null,
	})
	lastLogined: Date;

}
