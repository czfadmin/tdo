import { Observable, of } from "rxjs";

export interface CommonResult {
	code: number;
	data: any;
	message: string;
}
export class CommonResult implements CommonResult {

	static Success(data: any, message?: string): Observable<CommonResult> {
		return of({
			code: 2000,
			data: data,
			message: message
		});
	}

	static Failed(message: string): Observable<CommonResult> {
		return of({
			code: 4000,
			message: message,
			data: null
		})
	}

	static Forbidden(message: string): Observable<CommonResult> {
		return of({
			code: 403,
			message: message,
			data: null
		})
	}

	static UnAuthorization(message: string): Observable<CommonResult> {
		return of({
			code: 401,
			data: null,
			message: message
		})
	}

}