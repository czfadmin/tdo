export class CreateTodoDto {
	title: string;
	content: string;
	memo: string;
}
