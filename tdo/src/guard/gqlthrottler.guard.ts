
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { ThrottlerGuard } from '@nestjs/throttler';
import { Observable } from 'rxjs';


/**
 * GraphQL Throttler
 */
@Injectable()
export class GqlthrottlerGuard extends ThrottlerGuard {
	// getRequestResponse(context: ExecutionContext) {
	// 	// const gqlCtx = GqlExecutionContext.create(context);
	// 	// const ctx = gqlCtx.getContext();
	// 	// return { req: ctx.req, res: ctx.res }
	// }
}
